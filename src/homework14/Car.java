package homework14;

import java.util.Objects;

public class Car{
    private Integer number;
    private String modelName;
    private String color;
    private Integer mileage;
    private Integer price;

    public Car(Integer number, String modelName, String color, Integer mileage, Integer price) {
        this.number = number;
        this.modelName = modelName;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(number, car.number) && Objects.equals(modelName, car.modelName) && Objects.equals(color, car.color) && Objects.equals(mileage, car.mileage) && Objects.equals(price, car.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, modelName, color, mileage, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "number=" + number +
                ", modelName='" + modelName + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

    public static int compare (Car firstCar, Car secondCar){
        if(firstCar.getPrice() > secondCar.getPrice()) {
            return 1;
        }else if (firstCar.getPrice() < secondCar.getPrice()){
            return -1;
        } else{
            return 0;
        }
    }
}
