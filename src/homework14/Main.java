package homework14;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws Exception {

        BufferedReader firstReader = new BufferedReader(new FileReader("cars_db.txt"));
        firstReader.lines()
                .map((String line) -> { //можем сделать явное преобразование в стринг, но вообще тут не надо
                    String[] parsedLine = line.split("#");
                    return new Car(Integer.parseInt(parsedLine[0]), parsedLine[1], parsedLine[2], Integer.parseInt(parsedLine[3]),Integer.parseInt(parsedLine[4]));
                }).filter(car -> car.getColor().equals("Black") | car.getMileage().equals(0))
                .forEach(car -> System.out.println("Номер авто черного цвета или с нулевым пробегом: \n" + car.getNumber()));
        firstReader.close();


        BufferedReader secondReader = new BufferedReader(new FileReader("cars_db.txt"));
            List<Car> list = secondReader.lines()
                    .map(line -> {
                        String[] parsedLine = line.split("#");
                        return new Car(Integer.parseInt(parsedLine[0]), parsedLine[1], parsedLine[2], Integer.parseInt(parsedLine[3]), Integer.parseInt(parsedLine[4]));
                    }).filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                    .collect(Collectors.toList());
            //вопрос: после filter() хотел продолжить map(Car::getModelName).distinct().count()
            //но не получалось. В чем была ошибка так и не понял?
            long integerList =  list.stream().map(Car::getModelName).distinct().count();
            System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс:\n" + integerList);
            secondReader.close();

        BufferedReader thirdReader = new BufferedReader(new FileReader("cars_db.txt"));
        Car car = thirdReader.lines()
                    .map(line -> {
                        String[] parsedLine = line.split("#");
                        return new Car(Integer.parseInt(parsedLine[0]), parsedLine[1], parsedLine[2], Integer.parseInt(parsedLine[3]), Integer.parseInt(parsedLine[4]));
                    }).min(Comparator.comparing(Car::getPrice)).get();
        System.out.println("Цвет авто с минимальной стоимостью: \n" + car.getColor());
        thirdReader.close();

        BufferedReader fourthReader = new BufferedReader(new FileReader("cars_db.txt"));
        List<Car> listOfCamry = fourthReader.lines()
                .map(line -> {
                    String[] parsedLine = line.split("#");
                    return new Car(Integer.parseInt(parsedLine[0]), parsedLine[1], parsedLine[2], Integer.parseInt(parsedLine[3]), Integer.parseInt(parsedLine[4]));
                }).filter(car1 -> car1.getModelName().equals("Camry"))
                .collect(Collectors.toList());
        long count = listOfCamry.stream().map(Car::getNumber).distinct().count();
        long price = 0;
        for (Car carCamry : listOfCamry){
            price = price + carCamry.getPrice();
        }
        long average = price / count;
        System.out.println("Средняя стоимость Camry: \n" + average);

        //второе решение 4 задания
        double averageSecondMethod = listOfCamry.stream().map(Car::getPrice).mapToInt(Integer::intValue).average().getAsDouble();
        System.out.println("Средняя стоимость Camry: \n" + averageSecondMethod);

    }
}
