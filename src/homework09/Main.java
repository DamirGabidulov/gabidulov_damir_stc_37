package homework09;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String[] stringArray = new String[]{"MontanaCans", "georgia", "Crew12Member"};
        int[] intArray = new int[]{12345, 105, 320150};

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(intArray, stringArray);

        //Реализация 1 метода int
        NumbersProcess numbersProcess = new NumbersProcess() {
            @Override
            public int process(int number) {
                int result = 0;
                while (number > 0){
                    result = (result * 10) + number % 10;
                    number = number / 10;
                }
                return result;
            }
        };
        //первый вариант распечатки массива
        System.out.println(Arrays.toString(numbersAndStringProcessor.processOfNumbers(numbersProcess)));
        //второй вариант распечатки массива
        //поле numbersAndStringProcessor.ProcessOfNumbers(numbersProcess) означает вызов функции ProcessOfNumbers в классе numbersAndStringProcessor
        //в свою очередь эта функция возвращает измененный массив и чтобы вывести его значения нужно создать тестовый массив и его уже просить печатать свои элементы
        int[] testArray1 = numbersAndStringProcessor.processOfNumbers(numbersProcess);
        for (int i = 0; i < testArray1.length; i++)
            System.out.print(testArray1[i] + " ");
        System.out.println();


        //Реализация 2 метода int
        //здесь сократил до лямбды, предыдущий для наглядности в полном формате записан
        NumbersProcess numbersProcess1 = number -> {
            int result = 0;
            while (number > 0){
                if ((number % 10) == 0){
                    number = number / 10;
                } else {
                    result = (result * 10) + (number % 10);
                    number = number / 10;
                }
            }
            //теперь обратный порядок переворачиваю в нормальный
            int result1 = 0;
            while (result > 0){
                result1 = (result1 * 10) + result % 10;
                result = result / 10;
            }
            return result1;
        };

        int[] testArray2 = numbersAndStringProcessor.processOfNumbers(numbersProcess1);
        for (int i = 0; i < testArray2.length; i++)
            System.out.print(testArray2[i] + " ");
        System.out.println();


        //Реализация 3 метода int
        NumbersProcess numbersProcess2 = number -> {
            int result = 0;
            while (number > 0){
                if (((number % 10) % 2) == 0){
                    result = (result * 10) + (number % 10);
                    number = number / 10;
                } else if ((number % 10) == 0 || (number % 10) == 1){ //если убрать весь этот else if то 1 заменит на ноль,
                    result = (result * 10) + (number % 10);
                    number = number / 10;
                } else {
                    result = (result * 10) + ((number % 10) - 1);
                    number = number / 10;
                }
            }
            //теперь обратный порядок переворачиваю в нормальный
            int result1 = 0;
            while (result > 0){
                result1 = (result1 * 10) + result % 10;
                result = result / 10;
            }
            return result1;
        };

        int[] testArray3 = numbersAndStringProcessor.processOfNumbers(numbersProcess2);
        for (int i = 0; i < testArray3.length; i++)
            System.out.print(testArray3[i] + " ");
        System.out.println();


        //Реализация 1 метода String
        StringProcess stringProcess = new StringProcess() {
            @Override
            public String process(String string) {
                String result = "";
                for (int i = 0; i < string.length(); i++) {
                    //string.charAt(i) перед result так как сначала идет последняя буква а добавляется предыдущая, если поменять местами то выведет изначальную строку
                    result = string.charAt(i) + result;
                }
                return result;
            }
        };

        String[] testArray4 = numbersAndStringProcessor.processOfString(stringProcess);
        for (int i = 0; i < testArray4.length; i++)
            System.out.print(testArray4[i] + " ");
        System.out.println();


        //Реализация 2 метода String
        StringProcess stringProcess1 = string -> {
            String result = string.replaceAll("\\d","");
            return result;
        };

        String[] testArray5 = numbersAndStringProcessor.processOfString(stringProcess1);
        for (int i = 0; i < testArray5.length; i++)
            System.out.print(testArray5[i] + " ");
        System.out.println();


        //Реализация 3 метода String
        StringProcess stringProcess2 = string -> {
                return string.toUpperCase();
        };

        String[] testArray6 = numbersAndStringProcessor.processOfString(stringProcess2);
        for (int i = 0; i < testArray6.length; i++)
            System.out.print(testArray6[i] + " ");
        System.out.println();
    }
}
