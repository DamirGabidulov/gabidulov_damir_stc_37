package homework09;

public class NumbersAndStringProcessor {

    private String[] arrayOfString;
    private int[] arrayOfNumbers;

    public NumbersAndStringProcessor(int[] arrayOfNumbers, String[] arrayOfString) {
        this.arrayOfNumbers = arrayOfNumbers;
        this.arrayOfString = arrayOfString;
    }

    public String[] processOfString (StringProcess function) {
        String[] arrayOfStrings = new String[arrayOfString.length]; // как будто в скобках processOfString.length не нужен
        for (int i = 0; i < arrayOfString.length; i++){
            arrayOfStrings[i] = function.process(arrayOfString[i]); //так как в интерфейсе homework09.StringProcess в метод process передается только строка, а не весь массив например, то можно его отправить так processOfString[i]
        }
        return arrayOfStrings;
    }

    public int[] processOfNumbers(NumbersProcess function) {
        int[] newArrayOfNumbers = new int[arrayOfNumbers.length]; // как будто в скобках processOfNumbers.length не нужен
        for (int i = 0; i < arrayOfNumbers.length; i++){
            newArrayOfNumbers[i] = function.process(arrayOfNumbers[i]); //так как в интерфейсе NumberProcess в метод process передается только число, а не весь массив например, то можно его отправить так processOfNumbers[i]
        }
        return newArrayOfNumbers;
    }
}
