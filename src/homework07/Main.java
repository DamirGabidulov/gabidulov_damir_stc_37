package homework07;

public class Main {

    public static void main(String[] args) {
//        homework07.User user = homework07.User.builder()
        User user = new User.Builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();

        User user1 = new User.Builder()
                .lastName("Sidikov")
                .firstName("Marsel")
                .isWorker(true)
                .build();

//        homework07.User user2 = new homework07.User("a", "b", 8,true); /так сделать не получится, т.к. конструктор homework07.User теперь private

        System.out.println(user.toString());
        System.out.println(user1);
    }
}
