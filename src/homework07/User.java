package homework07;

public class User {

    private String firstname;
    private String lastname;
    private int age;
    private boolean isWorker;

    @Override
    public String toString() {
        return "homework07.User{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    private User(String firstname, String lastname, int age, boolean isWorker) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.isWorker = isWorker;
    }

    public static class Builder {

        private String firstname = "";
        private String lastname = "";
        private int age = 0;
        private boolean isWorker = false;


        public Builder firstName(String firstNameMain) {
            this.firstname = firstNameMain;
            return this;
        }

        public Builder lastName(String lastNameMain) {
            this.lastname = lastNameMain;
            return this;
        }

        public Builder age(int ageMain) {
            this.age = ageMain;
            return this;
        }

        public Builder isWorker(boolean isWorkerMain) {
            this.isWorker = isWorkerMain;
            return this;
        }

//        public homework07.User build() {
//            return new homework07.User(this);

//        }
        public User build() {
            return new User(this.firstname, this.lastname, this.age, this.isWorker);
        }

    }
//    private homework07.User (Builder builder) {
//        firstname = builder.firstname;
//        lastname = builder.lastname;
//        age = builder.age;
//        isWorker = builder.isWorker;
//    }
}
