import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Player;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;


import javax.sql.DataSource;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MainSimple {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Terminal1488";

    //language=SQL
    private static final String SQL_SELECT_PLAYER = "select * from player";

    public static void main(String[] args) throws Exception {

        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/java_stc37");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("Terminal1488");
        configuration.setMaximumPoolSize(20);

        DataSource dataSource = new HikariDataSource(configuration);

        PlayersRepositoryJDBCImpl playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        List<Player> players = playersRepository.findAll();
        System.out.println(players);

        System.out.println(playersRepository.findByNickname("r8"));


        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        System.out.println(gamesRepository.findById(33L));

        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();


        for (int j = 0; j < 3; j++) {
            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;

            int i = 0;
            while (i < 10) {

                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }

            //закоментировал чтобы убрать ошибку передачи времени в игру через сервер а не через service
//            StatisticDto statistic = gameService.finishGame(gameId);
//            System.out.println(statistic);

        }

    }
}
