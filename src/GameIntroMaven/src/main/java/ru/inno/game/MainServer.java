package ru.inno.game;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repository.*;
import ru.inno.game.server.GameServer;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {
    public static void main(String[] args) {
        // ConnectionPool - пул соединений с базой данных
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/java_stc37");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("Terminal1488");
        configuration.setMaximumPoolSize(20);
        // создали DataSource - источник данных
        DataSource dataSource = new HikariDataSource(configuration);
        // создаем репозиторий, который использует этот источник данных
        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        // создали сервис, который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository,gamesRepository, shotsRepository);
        // передали сервис объекту-серверу для нашей игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
