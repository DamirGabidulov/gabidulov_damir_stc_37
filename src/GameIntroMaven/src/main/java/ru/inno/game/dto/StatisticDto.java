package ru.inno.game.dto;

import ru.inno.game.models.Player;

import java.time.LocalDateTime;

// информация об игре
public class StatisticDto {

    private Long gameId;
    private String firstPlayerName;
    private String secondPlayerName;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private Integer firstPlayerPoints;
    private Integer secondPlayerPoints;
    private String winner;
    private Long secondsGameTimeAmount;

    public StatisticDto(Long gameId, String firstPlayerName, String secondPlayerName, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Integer firstPlayerPoints, Integer secondPlayerPoints, String winner, Long secondsGameTimeAmount) {
        this.gameId = gameId;
        this.firstPlayerName = firstPlayerName;
        this.secondPlayerName = secondPlayerName;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.firstPlayerPoints = firstPlayerPoints;
        this.secondPlayerPoints = secondPlayerPoints;
        this.winner = winner;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    @Override
    public String toString() {
        return "Игра с ID = " + gameId + ";"
                + "Игрок 1: " + firstPlayerName + ", попаданий - " + playerFirstShotsCount + ", всего очков - " + firstPlayerPoints + ";"
                + "Игрок 2: " + secondPlayerName + ", попаданий - " + playerSecondShotsCount + ", всего очков - " + secondPlayerPoints + ";"
                + winner + ";"
                + "Игра длилась: " + secondsGameTimeAmount + " секунд";
    }
}
