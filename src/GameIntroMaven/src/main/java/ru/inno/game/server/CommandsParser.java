package ru.inno.game.server;

public class CommandsParser {
    public static boolean isMessageForDamage(String messageFromClient) {
        return messageFromClient.equals("damage");
    }

    public static boolean isMessageForExit(String messageFromClient) {
        return messageFromClient.equals("exit");
    }

    public static boolean isMessageForMove(String messageFromClient) {
        return messageFromClient.equals("left") || messageFromClient.equals("right") || messageFromClient.equals("up");
    }

    public static boolean isMessageForShot(String messageFromClient){
        return messageFromClient.equals("shot");
    }
}
