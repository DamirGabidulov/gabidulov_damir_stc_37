package ru.inno.game.repository;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryJDBCImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT_SHOT = "insert into " +
            "shot(datetime, game_id, shooter, target) values (?, ?, ?, ?)";

    private DataSource dataSource;

    public ShotsRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    shot.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
