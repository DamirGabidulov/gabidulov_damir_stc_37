package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryJDBCImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into " +
            "game(datetime, player_first, player_second, player_first_shots_count, player_second_shots_count, seconds_game_time_amount) " +
            "values (?, ?, ?, ?, ?, ?)";

    //language=SQL
//    private static final String SQL_FIND_BY_ID_GAME = "select * from game where id = ?";

//    //language=SQL
//    private static final String SQL_FIND_BY_ID_GAME = "select g.id as game_id, " +
//            "g.dateTime as datetime, " +
//            "p.id as player_first_id, " +
//            "p.ip as playerfirst_ip," +
//            "p.name as player_first_name," +
//            "p.points as player_first_points," +
//            "p.maxLosesCount as  " +
//            "from game as g " +
//            "join player as p on g.player_first = p.id";

    //language=SQL
        private static final String SQL_FIND_BY_ID_GAME = "select *" +
            "from game g " +
            "inner join player p on p.id = g.player_first " +
            "inner join player p2 on p2.id = g.player_second where g.id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set " +
            "datetime = ?, player_first_shots_count = ?, player_second_shots_count = ?, seconds_game_time_amount = ? where id = ?";

    //language=SQL
    private static final String SQL_FIND_BY_ID_PLAYER = "select * from player where id = ?";

    static private final RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("player_name"))
            .points(row.getInt("points"))
            .maxWinsCount( row.getInt("max_wins_count"))
            .maxLosesCount(row.getInt("max_loses_count"))
            .build();


    private Player findPlayerById(Long id){
        Player player = null;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID_PLAYER)){
            statement.setLong(1, id);
            try (ResultSet rows = statement.executeQuery();) {
                if (rows.next()){
                    player = playerRowMapper.mapRow(rows);
                }
            }
        }catch (SQLException e) {
            throw new IllegalStateException(e);
        } return player;
    }

    private DataSource dataSource;
    public GamesRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Game game) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setObject(1,game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6,game.getSecondsGameTimeAmount());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedId = statement.getGeneratedKeys();) {
                if (generatedId.next()){
                    game.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            } catch (SQLException e){
                throw new IllegalStateException(e);
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {
        Game game = null;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID_GAME)) {
            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery();) {
                if (rows.next()) {
                    game = Game.builder()
                            .id(rows.getLong("id"))
                            .dateTime(rows.getTimestamp("datetime").toLocalDateTime())
                            .playerFirst(findPlayerById(rows.getLong("player_first")))
                            .playerSecond(findPlayerById(rows.getLong("player_second")))
                            .playerFirstShotsCount(rows.getInt("player_first_shots_count"))
                            .playerSecondShotsCount(rows.getInt("player_second_shots_count"))
                            .secondsGameTimeAmount(rows.getLong("seconds_game_time_amount"))
                            .build();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return game;
    }


    @Override
    public void update(Game game) {

        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setObject(1,game.getDateTime());
            statement.setInt(2, game.getPlayerFirstShotsCount());
            statement.setInt(3,game.getPlayerSecondShotsCount());
            statement.setLong(4, game.getSecondsGameTimeAmount());
            statement.setLong(5, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }
}
