package ru.inno.game.repository;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class PlayersRepositoryJDBCImpl implements PlayersRepository {
    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into " +
            "player(ip, player_name, points, max_wins_count, max_loses_count)" +
            "values (?, ?, ?, ?, ?)";


    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where player_name = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_PLAYERS = "select id as player_id, * from player";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER = "update player set ip = ?, player_name = ?, points = ?, max_wins_count = ?, max_loses_count = ? where id = ?";

    //старая реализация
//    static private final RowMapper<Player> playerRowMapper = row -> new Player(
//            row.getLong("id"),
//            row.getString("ip"),
//            row.getString("player_name"),
//            row.getInt("points"),
//            row.getInt("max_wins_count"),
//            row.getInt("max_loses_count")
//    );

    static private final RowMapper<Player> playerRowMapper = PlayersRepositoryJDBCImpl::mapRow;

    private DataSource dataSource;
    public PlayersRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static Player mapRow(ResultSet row) throws SQLException {
        return Player.builder()
                .id(row.getLong("id"))
                .ip(row.getString("ip"))
                .name(row.getString("player_name"))
                .points(row.getInt("points"))
                .maxWinsCount(row.getInt("max_wins_count"))
                .maxLosesCount(row.getInt("max_loses_count"))
                .build();
    }


    @Override
    public Player findByNickname(String nickname) {
        Player player = null;

        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME);) {
            statement.setString(1,nickname);
            try (ResultSet rows = statement.executeQuery();) {
                if (rows.next()){
                    player = playerRowMapper.mapRow(rows);
                }
            }
            return player;
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void save(Player player) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5, player.getMaxLosesCount());

            //сколько строк обновили
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    player.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }catch (SQLException e){
                throw new IllegalStateException(e);
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void update(Player player) {

        try (Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER);) {
            statement.setString(1,player.getIp());
            statement.setString(2,player.getName());
            statement.setInt(3,player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5,player.getMaxLosesCount());
            statement.setLong(6,player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }


    public List<Player> findAll(){

        List<Player> players = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement()) {
            try (ResultSet rows = statement.executeQuery(SQL_SELECT_ALL_PLAYERS)) {
                while (rows.next()){
                    Player player = playerRowMapper.mapRow(rows);
                    players.add(player);
                }
            }catch (SQLException e){
                throw new IllegalStateException(e);
            }

        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }return players;
    }
}
