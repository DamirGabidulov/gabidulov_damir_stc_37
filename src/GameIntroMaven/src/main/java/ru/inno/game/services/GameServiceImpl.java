package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;


// бизнес-логика
public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

//    private Long gameStartTime; //возможный вариант без обращения к репозиторию для вывода времени игры

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
//        System.out.println("Получили" + firstIp + " " + secondIp + " " + firstPlayerNickname + " " + secondPlayerNickname);
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
//        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);

        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();

        // сохранили игру в репозитории
        gamesRepository.save(game);
//        gameStartTime = System.currentTimeMillis(); //возможный вариант без обращения к репозиторию для вывода времени игры
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
//            player = new Player(ip, nickname, 0, 0, 0);
            player = Player.builder()
                    .ip(ip)
                    .name(nickname)
                    .points(0)
                    .maxWinsCount(0)
                    .maxLosesCount(0)
                    .build();

            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
//        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);

        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();

        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
        int i = 0;
    }

    @Override
    public StatisticDto finishGame(Long gameId, long seconds) {
        //логику подсчета времени убрали в сервер, чтобы сервис только обрабатывал данные а не хранил и подсчитывал время каждой игры
        //теперь этим занимается сервер

        Game game = gamesRepository.findById(gameId);
        Player firstPlayer = playersRepository.findByNickname(game.getPlayerFirst().getName());
        Player secondPlayer = playersRepository.findByNickname(game.getPlayerSecond().getName());
        game.setSecondsGameTimeAmount(seconds);
        String winner = findWinner(game);
        StatisticDto statisticDto = new StatisticDto(gameId, firstPlayer.getName(), secondPlayer.getName(), game.getPlayerFirstShotsCount(), game.getPlayerSecondShotsCount(),
                game.getPlayerFirst().getPoints(), game.getPlayerSecond().getPoints(), winner, seconds);
        gamesRepository.update(game);
        return statisticDto;


        //        System.out.println(firstPlayer.getName());
//        System.out.println((System.currentTimeMillis() - gameStartTime) / 1000); //возможный вариант без обращения к репозиторию для вывода времени игры
//        LocalDateTime.now().minus(game.getDateTime()); //возможный вариант без обращения к репозиторию для вывода времени игры

        //рабочий вариант до того как логику подсчета времени убрали в сервер
//        Duration duration = Duration.between(game.getDateTime(),LocalDateTime.now());
//        System.out.println("Игра длилась: " + duration.getSeconds() + " секунд");
//        game.setSecondsGameTimeAmount(duration.getSeconds());
    }

    private String findWinner(Game game){
        Player firstPlayer = playersRepository.findByNickname(game.getPlayerFirst().getName());
        Player secondPlayer = playersRepository.findByNickname(game.getPlayerSecond().getName());

        if (game.getPlayerFirstShotsCount() == game.getPlayerSecondShotsCount()){
            return "Ничья";
        }else if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()){
            firstPlayer.setMaxWinsCount(firstPlayer.getMaxWinsCount() + 1);
            secondPlayer.setMaxLosesCount(secondPlayer.getMaxLosesCount() + 1);
            playersRepository.update(firstPlayer);
            playersRepository.update(secondPlayer);
            String winnerOne = "Победа: " + game.getPlayerFirst().getName();
            return winnerOne;
        } else {
            System.out.println("Победа: " + game.getPlayerSecond().getName());
            secondPlayer.setMaxWinsCount(secondPlayer.getMaxWinsCount() + 1);
            firstPlayer.setMaxLosesCount(firstPlayer.getMaxLosesCount() + 1);
            playersRepository.update(firstPlayer);
            playersRepository.update(secondPlayer);
            String winnerTwo = "Победа: " + game.getPlayerSecond().getName();
            return winnerTwo;
        }
    }
}
