package homework02;
import java.util.Scanner;
import java.util.Arrays;

public class Lesson02Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int temp;

            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }
            for (int i = array.length - 1; i > 0; i--){
                for (int j = 0; j < i; j++){
                    if (array[j] > array [j + 1]) {
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        System.out.println(Arrays.toString(array));
    }
}
