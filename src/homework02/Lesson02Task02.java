package homework02;

import java.util.Scanner;
import java.util.Arrays;
class Lesson02Task02 {

	public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	int temp;
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		for (int i = 0; i < array.length / 2; i++){
			temp = array [i];
			array[i] = array[array.length - 1 -i];
			array[array.length - 1 -i] = temp;
		}

		System.out.println(Arrays.toString(array));

	}
}