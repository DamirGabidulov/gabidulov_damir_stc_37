package homework02;

import java.util.Scanner;
import java.util.Arrays;
class Lesson02Task04 {

	public static void main(String args[]) {

	Scanner scanner = new Scanner(System.in);


	int n = scanner.nextInt();
	int array[] = new int[n];
	int x;
	int min = array[0];
	int max = array[0];
	int positionOfMin = 0;
	int positionOfMax = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] < min){
				min = array[i];
				positionOfMin = i;
			}
			if (array[i] > max){
				max = array[i];
				positionOfMax = i;
			}
		}

		x = array[positionOfMin];
		array[positionOfMin] = array[positionOfMax];
		array[positionOfMax] = x;

		System.out.println(Arrays.toString(array));


	}
}