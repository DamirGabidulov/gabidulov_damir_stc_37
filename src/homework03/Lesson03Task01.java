package homework03;

import java.util.Scanner;
import java.util.Arrays;

public class Lesson03Task01 {

        public static int arraySum(int array[]) {
            int x;
            int arraySum = 0;

            for (int i = 0; i < array.length; i++) {
                x = array[i];
                arraySum = x + arraySum;
            }
            return arraySum;
        }

        public static void arraysMirror(int array[]) {
            int temp;

            int array2[] = new int [array.length];
            int m;
            int position;
            for (int i = 0; i < array.length; i++){
                m = array[i];
                position = i;
                array2[position] = m;
            }

            for (int i = 0; i < array2.length / 2; i++){
                temp = array2 [i];
                array2[i] = array2[array2.length - 1 -i];
                array2[array2.length - 1 -i] = temp;
            }

            System.out.println(Arrays.toString(array2));
        }

        public static void arrayAverage(int array[]) {

            double arrayAverage = 0;

            double arraySum2 = arraySum(array);
            arrayAverage = arraySum2 / array.length;

            System.out.println(arrayAverage);
        }

        public static void changeMinToMax(int array[]) {

            int array3[] = new int [array.length];
            int m;
            int position;
            for (int i = 0; i < array.length; i++){
                m = array[i];
                position = i;
                array3[position] = m;
            }

            int step;
            int min = array3[0];
            int max = array3[0];
            int positionOfMin = 0;
            int positionOfMax = 0;


            for (int i = 0; i < array3.length; i++) {
                if (array3[i] < min){
                    min = array3[i];
                    positionOfMin = i;
                }
                if (array3[i] > max){
                    max = array3[i];
                    positionOfMax = i;
                }
            }

            step = array3[positionOfMin];
            array3[positionOfMin] = array3[positionOfMax];
            array3[positionOfMax] = step;

            System.out.println(Arrays.toString(array3));
        }

        public static void bubbleSort(int array[]) {

            int array4[] = new int [array.length];
            int m;
            int position;
            for (int i = 0; i < array.length; i++){
                m = array[i];
                position = i;
                array4[position] = m;
            }

            int temp;
            for (int i = array4.length - 1; i > 0; i--){
                for (int j = 0; j < i; j++){
                    if (array4[j] > array4 [j + 1]) {
                        temp = array4[j];
                        array4[j] = array4[j + 1];
                        array4[j + 1] = temp;
                    }
                }
            }
            System.out.println(Arrays.toString(array4));
        }

        public static void arrayToNumber(int array[]) {
            int[] array5 = new int [array.length];
            int m;
            int position;
            for (int i = 0; i < array.length; i++){
                m = array[i];
                position = i;
                array5[position] = m;
            }

            int x;
            int y = 1;

            int number = array5[array5.length - 1];

            for (int i = array5.length - 2; i >=0; i--){
                x = array5[i];
                y = y * 10;
                number = x * y + number;
            }

            System.out.println(number);

        }

        public static void main(String args[]) {

            Scanner scanner = new Scanner(System.in);

            int n = scanner.nextInt();
            int array[] = new int[n];



            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }

            System.out.println("Сумма чисел равна");
            System.out.println(arraySum (array));
            System.out.println("Разворот массива");
            arraysMirror(array);
            System.out.println("Среднее арифметическое элементов массива");
            arrayAverage(array);
            System.out.println("Минимальное и максимальное число поменялись местами");
            changeMinToMax(array);
            System.out.println("Сортировка массива методом пузырька");
            bubbleSort(array);
            System.out.println("Преобразование массива в число");
            arrayToNumber(array);
        }
}

