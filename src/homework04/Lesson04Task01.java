package homework04;

public class Lesson04Task01 {
    public static void main(String[] args) {
        numberSquared(16);
    }

    public static void numberSquared(int n) {
        int m = n % 2;

        if (n == 1 || n == 2) {
            System.out.println("True");
            return;
        }
        if (m == 1) {
            System.out.println("False");
        } else {
            numberSquared(n / 2);
        }
    }
}