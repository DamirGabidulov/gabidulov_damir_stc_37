package homework04;

import java.util.Scanner;

public class Lesson04Task02 {
    public static boolean binarySearch(int array[], int element){
        return binarySearch2(array, element, 0, array.length - 1);
    }
    public static boolean binarySearch2(int array[], int element, int left, int right) {
        boolean isExists = false;
/*
        if (left == right){
            return isExists;
        }

 */
        int middle = left + (right - left)/2;
        while (left <= right) {
            if (array[middle] < element) {
                return binarySearch2(array, element, middle + 1, right);
            } else if (array[middle] > element) {
                return binarySearch2(array, element, left, middle - 1);
            } else if (array[middle] == element) {
                isExists = true;
                break;
            }
        }
            return  isExists;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int element = scanner.nextInt();

        int array[] = {10, 15, 20, 25, 30, 35, 40, 45, 50, 60};
        System.out.println(binarySearch(array, element));

    }
}
