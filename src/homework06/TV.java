package homework06;

import java.util.Random;

public class TV {
    private Channel channels[];
    private int channelsCount;

    public TV (Channel channels[]){
        this.channels = channels;
    }

    public TV() {
        this.channels = new Channel[3];
        this.channelsCount = 0; // а здесь массив ограничили количеством 3 штуки внутри конструктора, поэтому начальное кол-во равно нулю
    }


    public void addChannelsIn (Channel channel){
        if (this.channelsCount < channels.length) {
            this.channels[channelsCount] = channel;
            channelsCount++;
        } else {
            System.err.println("На сегодня телепередачи закончились!");
        }
    }


//    public void addChannelsIn (Channel channel){
//        for (int i = 0; i < channels.length; i++){ //если вызвать в мейне это вариант, то таким образом он забивает весь массив хоть одним каналом
//            channels[i] = channel;
//        }
//    }


    public void showChannel (int channelNumber) {
        if (channels.length <= channelNumber || channelNumber < 0){
            System.out.println("Такого канала не существует, выберите другой канал");
            return;
        }
        Channel selectChannel = channels[channelNumber];
      //  System.out.println("Советуем посмотреть программу " + programs[randomIndex].getProgramName());
        selectChannel.showRandomProgram();
    }

    public void channelName (int channelNum) {
        System.out.println("Сейчас Вы смотрите: " + channels[channelNum].getChannelName());
    }

}
