package homework06;

import java.util.Random;
public class Channel {
    private String channelName;
    private Program [] programs;
    private int programsCount;


    public Channel(String channelName, Program [] programs) {

        this.channelName = channelName;
        this.programs = programs;
        this.programsCount = programs.length; //так как из main пришел готовый массив, то кол-во программ равно длине массива

    }
    public Channel(String channelName) {

        this.channelName = channelName;
        this.programs = new Program[3];
        this.programsCount = 0; // а здесь массив ограничили количеством 3 штуки внутри конструктора, поэтому начальное кол-во равно нулю
    }


    public void addProgramIn (Program program){
        if (this.programsCount < programs.length) {
            this.programs[programsCount] = program;
            programsCount++;
        } else {
            System.err.println("На сегодня телепередачи закончились!");
        }
    }

    public void showRandomProgram () { // пустые скобки () потому что мне ничего на ввод передавать не нужно, достаточно обозначить к какому каналу этот метод относится в мейне
        if (programsCount == 0){  // это защита от пустого канала
            System.out.println("Канал пустой");
            return;
        }


        Random random = new Random();
        int randomIndex = random.nextInt(programsCount - 1); //Когда у нас например 3 элемента массива заполнено, то из каких индексов надо выбирать?  0,1,2   То есть нам нужно случайное число от 0 до 2. Count у нас 3, значит выбираем от 0 до 2 <3-1>
        System.out.println("Советуем посмотреть программу " + programs[randomIndex].getProgramName()); // изначально здесь было только programs[randomIndex], но так указывает только расположение элемента в памяти. Поэтому добавили метод getProgramName()
    }


    public String getChannelName() {
        return channelName;
    }
}
