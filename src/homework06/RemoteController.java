package homework06;

public class RemoteController {
    private TV tv;

    public RemoteController (TV tv) {
        this.tv = tv;
    }

    public void setTv (TV tv){
        this.tv = tv;
    }

    public void showTVChannel (int tvchannelNumber) {
        this.tv.channelName(tvchannelNumber);
    }

    public void likeShowChannelMethod (int channelNumber) {
        this.tv.showChannel(channelNumber);
    }
}
