package homework06;

public class Main {

    public static void main(String[] args) {
        Program program1 = new Program ("Доброе утро");
        Program program2 = new Program ("Новости");
        Program program3 = new Program ("Финал чемпионата по JAVA");
        Program program4 = new Program ("Как выжить в Иннополисе");
        Program program5 = new Program ("Вечерний Марсель");
        Program program6 = new Program ("100 вопросов наставнику");

        Program [] programs1 = new Program[] {program1, program2};

        Channel channel1 = new Channel("Первый канал", programs1);
        Channel channel2 = new Channel("Второй канал");
        Channel channel3 = new Channel("Третий канал");

        channel2.addProgramIn(program3);
        channel2.addProgramIn(program6);
        channel2.addProgramIn(program5);

        channel1.showRandomProgram();
        channel1.showRandomProgram();
        channel2.showRandomProgram();
        channel2.showRandomProgram();
        System.out.println();

        Channel [] mychannels = new Channel[] {channel1, channel1, channel2, channel3}; //создаю массив каналов перед созданием TV, так ему на вход требует массив

        TV myTV = new TV(mychannels);

//        TV myTV = new TV();
//        myTV.addChannelsIn(channel2); //можно добавить канал в тв самому, но не получается использовать оба варианта когда массив каналов задан и когда можно добавлять еще

        myTV.showChannel(0);
        myTV.showChannel(2);
        myTV.showChannel(1);
        myTV.showChannel(3); //если канал1 задан напрямую в мейне, канал2 через метод, то здесь выводит Канал пустой, потому что он вообще ничем не заполнен
        System.out.println();

        myTV.showChannel(4);
        myTV.showChannel(-1);
        System.out.println();

        RemoteController remoteController = new RemoteController(myTV);

        remoteController.showTVChannel(0);
        remoteController.showTVChannel(1);
        remoteController.showTVChannel(2);
        remoteController.showTVChannel(3);
        System.out.println();

        remoteController.likeShowChannelMethod(0);
    }
}
