package homework08;

public class Square extends Rectangle {
    private double side;

    public Square(double x, double y, double side) {
        super(x,y,side, side);
        }

    @Override
    public void scalable(double scale) {
        this.side = this.side * scale;
    }

    // здесь уже заново описывать методы не нужно, они работают как в предке
//    public void area(){
//        double area = firstSide * secondSide;
//        System.out.println(area);
//    }
//
//    public void perimetr(){
//        double perimetr = 2 * (firstSide + secondSide);
//        System.out.println(perimetr);
//    }
}
