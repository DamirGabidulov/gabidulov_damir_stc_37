package homework08;

public class Ellipse extends GeometricShapes {
    private double smallRadius;
    private double bigRadius;
    static final double PI = Math.PI;

    public Ellipse(double x, double y, double smallRadius, double bigRadius) {
        super(x,y);
        this.smallRadius = smallRadius;
        this.bigRadius = bigRadius;
    }

    public void area(){
        double area = PI * smallRadius * bigRadius;
        System.out.println("Площадь равна " + area);
    }

    @Override
    public void relocatable(double newX, double newY) {
        super.setX(newX);
        super.setY(newY);
    }

    @Override
    public void scalable(double scale) {
        this.bigRadius = this.bigRadius * scale;
        this.smallRadius = this.smallRadius * scale;
    }

    public void perimetr(){
        if (bigRadius != smallRadius) {
            double perimetr = 4 * (((PI * smallRadius * bigRadius) + Math.pow(bigRadius - smallRadius, 2)) / (bigRadius + smallRadius));
            System.out.println("Периметр равен " + perimetr);
        } else {
            double perimetr = 2 * PI * bigRadius;
            System.out.println("Периметр равен " + perimetr);
        }




    }
}
