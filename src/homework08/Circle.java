package homework08;

public class Circle extends Ellipse {
    private double radius;

    public Circle(double x, double y, double radius) {
        super(x, y, radius, radius);
    }

    @Override
    public void scalable(double scale) {
        this.radius = this.radius * scale;
    }




    //    public void perimetr (){
//        double perimetr = 2 * PI * radius;
//        System.out.println("Периметр равен " + perimetr);
//    }


//    private double radius;
//    private static final double PI = Math.PI;
//
//
//    public homework08.Circle(double x, double y, double radius) { //x и y нужно прописывать в скобках
//        super(x,y); //также и здесь нужно прописать х и у, почему-то super() не достаточно
//        this.radius = radius;
//    }
//
//    public void area (){
//        double area = PI * radius * radius;
//        System.out.println("Площадь равна " + area);;
//    }
//
//    public void perimetr (){
//        double perimetr = 2 * PI * radius;
//        System.out.println("Периметр равен " + perimetr);
//    }
}
