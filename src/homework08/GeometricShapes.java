package homework08;

public abstract class GeometricShapes implements Scalable, Relocatable {
    private double x;
    private double y;

    public GeometricShapes(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void area(){};

    public void perimetr(){};

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
