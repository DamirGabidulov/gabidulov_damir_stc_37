package homework08;

public class Main {

    public static void main(String[] args) {

        System.out.println("У круга:");
        Circle circle = new Circle(0,0,4);
        circle.area();
        circle.perimetr();
        System.out.println("Координаты центра: X=" + circle.getX() + ", Y=" + circle.getY());
        circle.relocatable(2,3);
        System.out.println("Новые координаты центра: X=" + circle.getX() + ", Y=" + circle.getY());
        circle.scalable(2);
        circle.area();
        circle.perimetr();
        System.out.println();

        System.out.println("У эллипса:");
        Ellipse ellipse = new Ellipse(0,0,2,3);
        ellipse.area();
        ellipse.perimetr();
        System.out.println("Координаты центра: X=" + ellipse.getX() + ", Y=" + ellipse.getY());
        ellipse.relocatable(3,4);
        System.out.println("Новые координаты центра: X=" + ellipse.getX() + ", Y=" + ellipse.getY());
        ellipse.scalable(2);
        ellipse.area();
        ellipse.perimetr();
        System.out.println();

        System.out.println("У прямоугольника:");
        Rectangle rectangle = new Rectangle(0,0,5,3);
        rectangle.area();
        rectangle.perimetr();
        System.out.println("Координаты центра: X=" + rectangle.getX() + ", Y=" + rectangle.getY());
        rectangle.relocatable(6,9);
        System.out.println("Новые координаты центра: X=" + rectangle.getX() + ", Y=" + rectangle.getY());
        rectangle.scalable(4);
        rectangle.area();
        rectangle.perimetr();
        System.out.println();

        System.out.println("У квадрата:");
        Square square = new Square(0,0,5);
        square.area();
        square.perimetr();
        System.out.println("Координаты центра: X=" + square.getX() + ", Y=" + square.getY());
        square.relocatable(6,9);
        System.out.println("Новые координаты центра: X=" + square.getX() + ", Y=" + square.getY());
        square.scalable(3);
        square.area();
        square.perimetr();
        System.out.println();
    }
}
