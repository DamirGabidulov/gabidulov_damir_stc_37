package homework08;

public class Rectangle extends GeometricShapes{
    private double firstSide;
    private double secondSide;

    public Rectangle(double x, double y, double firstSide, double secondSide) {
        super(x,y);
        this.firstSide = firstSide;
        this.secondSide = secondSide;
    }

    public void area(){
        double area = firstSide * secondSide;
        System.out.println("Площадь равна " + area);
    }

    public void perimetr(){
        double perimetr = 2 * (firstSide + secondSide);
        System.out.println("Периметр равен " + perimetr);
    }

    @Override
    public void relocatable(double newX, double newY) {
        super.setX(newX);
        super.setY(newY);
    }

    @Override
    public void scalable(double scale) {
        this.firstSide = this.firstSide * scale;
        this.secondSide = this.secondSide * scale;
    }
}
