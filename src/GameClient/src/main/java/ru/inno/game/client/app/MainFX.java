package ru.inno.game.client.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inno.game.client.controllers.MainController;

public class MainFX extends Application {

    public static void main(String[] args) {
        launch();
    }

    //The JavaFX Stage class is the top level JavaFX container.
    @Override
    public void start(Stage primaryStage) throws Exception {
        String fxmlFile = "/fxml/Main.fxml";
        FXMLLoader loader = new FXMLLoader();
        //просим загрузить файл. это делается с помощью getClass - получаем для текущего класса Main его ресурсы
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));

        primaryStage.setTitle("Game Client");
        //создали сцену
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("/css/application.css").toExternalForm());
        primaryStage.setScene(scene);
        //чтобы нельзя было менять размер
        primaryStage.setResizable(false);
        // получили контроллер
        MainController controller = loader.getController();
        // взяли обработчик нажатий из контроллера и добавили его в сцену
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());

        primaryStage.show();

    }
}
