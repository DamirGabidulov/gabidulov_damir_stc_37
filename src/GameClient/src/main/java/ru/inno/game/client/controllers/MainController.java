package ru.inno.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import ru.inno.game.client.socket.SocketClient;
import ru.inno.game.client.utils.GameUtils;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private SocketClient socketClient;
    private GameUtils gameUtils;

    @FXML
    private ImageView enemy;

    @FXML
    private ImageView player;

    @FXML
    private TextField textPlayerName;

    @FXML
    private Button buttonConnect;

    @FXML
    private Button buttonGo;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label hpPlayer;

    @FXML
    private Label hpEnemy;

    @FXML
    private Separator rightSeparator;

    @FXML
    private Separator leftSeparator;

    @FXML
    private Button buttonStop;

    @FXML
    private Label result;

    @FXML
    private ProgressBar firstProgressHp;

    @FXML
    private ProgressBar secondProgressHp;


    //для связи с мейном
    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }

    //обработчик события
    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
        //сравнивает код события с кодом кнопки right
        if (event.getCode() == KeyCode.RIGHT){
            gameUtils.goRight(player, false);
            socketClient.sendMessage("right");
        } else if (event.getCode() == KeyCode.LEFT){
            gameUtils.goLeft(player, false);
            socketClient.sendMessage("left");
        } else if (event.getCode() == KeyCode.SPACE){
            Circle bullet = gameUtils.createBullet(player, false);
            socketClient.sendMessage("shot");
        } else if (event.getCode() == KeyCode.UP){
            gameUtils.goUp(player);
            socketClient.sendMessage("up");
        }
    };



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gameUtils = new GameUtils();
        firstProgressHp.setStyle("-fx-accent: red");
        secondProgressHp.setStyle("-fx-accent: red");

        buttonConnect.setOnAction(event -> {
            socketClient = new SocketClient(this, "localhost", 7777);
            new Thread(socketClient).start();
            //разблокировка
            buttonConnect.setDisable(true);
            buttonGo.setDisable(false);
            textPlayerName.setDisable(false);
            gameUtils.setPane(pane);
            //положили в gameUtils клиента
            gameUtils.setClient(socketClient);
        });
        buttonGo.setOnAction(event -> {
            socketClient.sendMessage("name: " + textPlayerName.getText());
            buttonGo.setDisable(true);
            textPlayerName.setDisable(true);
            buttonStop.setDisable(false);
            buttonGo.getScene().getRoot().requestFocus();
        });
        buttonStop.setOnAction(event -> {
            socketClient.sendMessage("exit");
            buttonStop.setDisable(true);
            buttonConnect.setDisable(false);
        });

        //положили в gameUtils контроллер
        gameUtils.setMainController(this);

    }

    public ImageView getEnemy() {
        return enemy;
    }

    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public ImageView getPlayer() {
        return player;
    }

    public Label getHpPlayer() {
        return hpPlayer;
    }

    public Label getHpEnemy() {
        return hpEnemy;
    }

    public Separator getRightSeparator() {
        return rightSeparator;
    }

    public Separator getLeftSeparator() {
        return leftSeparator;
    }

    public Label getResult() {
        return result;
    }

    public ProgressBar getFirstProgressHp() {
        return firstProgressHp;
    }

    public ProgressBar getSecondProgressHp() {
        return secondProgressHp;
    }

    public Button getButtonStop() {
        return buttonStop;
    }

    public SocketClient getSocketClient() {
        return socketClient;
    }

    public Button getButtonConnect() {
        return buttonConnect;
    }

    //    @FXML
//    private Button buttonHello;
//
//    @FXML
//    private Label labelMessage;
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        //обработчик события
//        EventHandler<ActionEvent> buttonClickReaction = new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) {
//                labelMessage.setText("Hi!");
//            }
//        };
//        //привязали изменение labelMessage к кнопке buttonHello с помощью обработчика события
//        buttonHello.setOnAction(buttonClickReaction);


}

