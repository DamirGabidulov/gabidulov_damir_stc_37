package ru.inno.game.client.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

public class GameUtils {

    private static final int PLAYER_STEP = 5;
    private static final int DAMAGE = 5;

    private AnchorPane pane;
    private MainController mainController;
    private SocketClient client;

    public void goRight (ImageView player, boolean isEnemy){
        if (!isIntersectSeparator(mainController.getRightSeparator(), player)) {
            player.setX(player.getX() + PLAYER_STEP);
            //добавил для правильного отображения противника на экране
            if (!isEnemy){
                player.setRotate(90);
            } else {
                player.setRotate(-90);
            }
        }
    }

    public void goLeft (ImageView player, boolean isEnemy){
        if (!isIntersectSeparator(mainController.getLeftSeparator(), player))
        player.setX(player.getX() - PLAYER_STEP);
        if (!isEnemy){
            player.setRotate(-90);
        } else {
            player.setRotate(90);
        }
    }

    public void goUp (ImageView player) {
        player.setRotate(0);
    }

    // player - кто стреляет, isEnemy - false - мы стреляем во врага, isEnemy - true - враг в нас
    public Circle createBullet(ImageView player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(5);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getX() + player.getLayoutX() + (player.getFitWidth() / 2));
        bullet.setCenterY(player.getY() + player.getLayoutY());
        bullet.setFill(Color.GREEN);

        int value;

        if(isEnemy){
            value = 1;
        } else {
            value = -1;
        }

        final ImageView target;
        final Label targetHp;
        final ProgressBar targetProgressBar;

        // определяем, куда стреляем
        if (!isEnemy) {
            target = mainController.getEnemy();
            targetHp = mainController.getHpEnemy();
            targetProgressBar = mainController.getSecondProgressHp();
        } else {
            target = mainController.getPlayer();
            targetHp = mainController.getHpPlayer();
            targetProgressBar = mainController.getFirstProgressHp();
        }

        //анимация пули
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
            bullet.setCenterY(bullet.getCenterY() + value);
            // если пуля еще видна и произошло пересечение
            if (bullet.isVisible() && isIntersects(bullet, target)){
                // уменьшить здоровье
                createDamage(targetHp, targetProgressBar);
                //скрыть пулю
                bullet.setVisible(false);
                if (!isEnemy) {
                    client.sendMessage("damage");
                }
            }
        }));

        timeline.setCycleCount(500);
        timeline.play();
        return bullet;
    }

    private void createDamage(Label hpLabel, ProgressBar hpProgressBar){
        int hpPlayer = Integer.parseInt(hpLabel.getText());
        hpLabel.setText(String.valueOf(hpPlayer - DAMAGE));
        double hpp = Double.parseDouble(hpLabel.getText()) / 100;
        hpProgressBar.setProgress(hpp - ((double) DAMAGE / 100));
    }

    private boolean isIntersects(Circle bullet, ImageView player) {
        return bullet.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    private boolean isIntersectSeparator(Separator separator, ImageView player) {
        return separator.getBoundsInParent().intersects(player.getBoundsInParent());
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setClient(SocketClient client) {
        this.client = client;
    }
}
