package ru.inno.game.client.socket;

import javafx.application.Platform;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 22.04.2021
 * 38. Sockets IO - Client
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// поток для получения сообщений с сервера
public class SocketClient extends Thread {
    // канал подключения
    private Socket socket;
    // стрим для отправления сообщений серверу
    private PrintWriter toServer;
    // стрим для получения сообщений от сервера
    private BufferedReader fromServer;

    private MainController controller;

    private GameUtils gameUtils;

    public SocketClient(MainController controller, String host, int port) {
        try {
            // создаем подключение к серверу
            socket = new Socket(host, port);
            // получаем стримы для чтения и записи
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // ждем сообщений от сервера
    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            if (Integer.parseInt(controller.getHpPlayer().getText()) <= 0 || Integer.parseInt(controller.getHpEnemy().getText()) <= 0){
                sendMessage("exit");

            }
            try {
                // прочитали сообщение с сервера
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    if (messageFromServer.startsWith("Игра с")){
                        String[] parsedString = messageFromServer.split(";");
                        Platform.runLater(() -> controller.getResult().setText(parsedString[0] + "\n" + parsedString[1] + "\n" + parsedString[2] +"\n" + parsedString[3] + "\n" + parsedString[4]));
                    }
                    //поэтому добавили поле gameUtils и controller
                    else if (messageFromServer.equals("left")) {
                        gameUtils.goLeft(controller.getEnemy(), true);
                    } else if (messageFromServer.equals("right")){
                        gameUtils.goRight(controller.getEnemy(), true);
                    } else if (messageFromServer.equals("shot")){
                        //работает в потоке javafx
                        Platform.runLater(() -> gameUtils.createBullet(controller.getEnemy(), true));
                    } else if (messageFromServer.equals("up")){
                        gameUtils.goUp(controller.getEnemy());
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    // отправляем сообщение серверу
    public void sendMessage(String message) {
        toServer.println(message);
    }

}
