package GameIntro.models;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player shooter;
    private Player target;

    public Shot(LocalDateTime dateTime, Game game, Player shooter, Player target) {
        this.dateTime = dateTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getShooter() {
        return shooter;
    }

    public void setShooter(Player shooter) {
        this.shooter = shooter;
    }

    public Player getTarget() {
        return target;
    }

    public void setTarget(Player target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(dateTime, shot.dateTime) &&
                Objects.equals(game, shot.game) &&
                Objects.equals(shooter, shot.shooter) &&
                Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, game, shooter, target);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Shot.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateTime=" + dateTime)
                .add("game=" + game.getId())
                .add("shooter=" + shooter.getName())
                .add("target=" + target.getName())
                .toString();
    }

//    public static class Builder {
//        private Long id = null;
//        private LocalDateTime dateTime = null;
//        private Game game = null;
//        private Player shooter = null;
//        private Player target = null;
//
//
//        public Builder id(Long id) {
//            this.id = id;
//            return this;
//        }
//
//        public Builder dateTime(LocalDateTime dateTime) {
//            this.dateTime = dateTime;
//            return this;
//        }
//
//        public Builder game(Game game) {
//            this.game = game;
//            return this;
//        }
//
//        public Builder shooter(Player shooter) {
//            this.shooter = shooter;
//            return this;
//        }
//
//        public Builder target(Player target) {
//            this.target = target;
//            return this;
//        }
//
//        public Shot build() {
//            return new Shot(this.id, this.dateTime, this.game, this.shooter, this.target);
//        }
//    }
}
