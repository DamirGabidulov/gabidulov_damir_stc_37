package GameIntro.models;


import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

public class Game {
    private Long id;
    private LocalDateTime dateTime;
    private Player playerFirst;
    private Player playerSecond;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private Long secondsGameTimeAmount;

    public Game(LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Long secondsGameTimeAmount) {
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Game(Long id, LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Long secondsGameTimeAmount) {
        this.id = id;
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Player getPlayerFirst() {
        return playerFirst;
    }

    public void setPlayerFirst(Player playerFirst) {
        this.playerFirst = playerFirst;
    }

    public Player getPlayerSecond() {
        return playerSecond;
    }

    public void setPlayerSecond(Player playerSecond) {
        this.playerSecond = playerSecond;
    }

    public Integer getPlayerFirstShotsCount() {
        return playerFirstShotsCount;
    }

    public void setPlayerFirstShotsCount(Integer playerFirstShotsCount) {
        this.playerFirstShotsCount = playerFirstShotsCount;
    }

    public Integer getPlayerSecondShotsCount() {
        return playerSecondShotsCount;
    }

    public void setPlayerSecondShotsCount(Integer playerSecondShotsCount) {
        this.playerSecondShotsCount = playerSecondShotsCount;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(dateTime, game.dateTime) &&
                Objects.equals(playerFirst, game.playerFirst) &&
                Objects.equals(playerSecond, game.playerSecond) &&
                Objects.equals(playerFirstShotsCount, game.playerFirstShotsCount) &&
                Objects.equals(playerSecondShotsCount, game.playerSecondShotsCount) &&
                Objects.equals(secondsGameTimeAmount, game.secondsGameTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, playerFirst, playerSecond, playerFirstShotsCount, playerSecondShotsCount, secondsGameTimeAmount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Game.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateTime=" + dateTime)
                .add("playerFirst=" + playerFirst.getName())
                .add("playerSecond=" + playerSecond.getName())
                .add("playerFirstShotsCount=" + playerFirstShotsCount)
                .add("playerSecondShotsCount=" + playerSecondShotsCount)
                .add("secondsGameTimeAmount=" + secondsGameTimeAmount)
                .toString();
    }

    public static class Builder{
        private Long id = null;
        private LocalDateTime dateTime = null;
        private Player playerFirst = null;
        private Player playerSecond = null;
        private Integer playerFirstShotsCount = 0;
        private Integer playerSecondShotsCount = 0;
        private Long secondsGameTimeAmount = null;

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder dateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public Builder playerFirst(Player playerFirst) {
            this.playerFirst = playerFirst;
            return this;
        }

        public Builder playerSecond(Player playerSecond) {
            this.playerSecond = playerSecond;
            return this;
        }

        public Builder playerFirstShotsCount(Integer playerFirstShotsCount) {
            this.playerFirstShotsCount = playerFirstShotsCount;
            return this;
        }

        public Builder playerSecondShotsCount(Integer playerSecondShotsCount) {
            this.playerSecondShotsCount = playerSecondShotsCount;
            return this;
        }

        public Builder secondsGameTimeAmount(Long secondsGameTimeAmount) {
            this.secondsGameTimeAmount = secondsGameTimeAmount;
            return this;
        }
        public Game build() {
            return new Game(this.id, this.dateTime, this.playerFirst, this.playerSecond, this.playerFirstShotsCount,
                    this.playerSecondShotsCount,  this.secondsGameTimeAmount);
        }
    }
}
