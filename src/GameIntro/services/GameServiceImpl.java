package GameIntro.services;

import GameIntro.dto.StatisticDto;
import GameIntro.models.Game;
import GameIntro.models.Player;
import GameIntro.models.Shot;
import GameIntro.repository.GamesRepository;
import GameIntro.repository.PlayersRepository;
import GameIntro.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;


// бизнес-логика
public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

//    private Long gameStartTime; //возможный вариант без обращения к репозиторию для вывода времени игры

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);
        // сохранили игру в репозитории
        gamesRepository.save(game);
//        gameStartTime = System.currentTimeMillis(); //возможный вариант без обращения к репозиторию для вывода времени игры
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
//        Shot shot = new Shot.Builder()
//                .dateTime(LocalDateTime.now())
//                .game(game)
//                .shooter(shooter)
//                .target(target)
//                .build();

        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
        int i = 0;
    }

    @Override
    public StatisticDto finishGame(Long gameId) {

        System.out.println("Игра с ID = " + gameId);
        Game game = gamesRepository.findById(gameId);
        Player firstPlayer = playersRepository.findByNickname(game.getPlayerFirst().getName());
        Player secondPlayer = playersRepository.findByNickname(game.getPlayerSecond().getName());
        System.out.println("Игрок 1: " + game.getPlayerFirst().getName() + ", попаданий - " + game.getPlayerFirstShotsCount() + ", всего очков - " + game.getPlayerFirst().getPoints());
        System.out.println("Игрок 2: " + game.getPlayerSecond().getName() + ", попаданий - " + game.getPlayerSecondShotsCount() + ", всего очков - " + game.getPlayerSecond().getPoints());

        if (game.getPlayerFirstShotsCount() == game.getPlayerSecondShotsCount()){
            System.out.println("Ничья");
        }else if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()){
            System.out.println("Победа: " + game.getPlayerFirst().getName());
            firstPlayer.setMaxWinsCount(firstPlayer.getMaxWinsCount() + 1);
            secondPlayer.setMaxLosesCount(secondPlayer.getMaxLosesCount() + 1);
        } else {
            System.out.println("Победа: " + game.getPlayerSecond().getName());
            secondPlayer.setMaxWinsCount(secondPlayer.getMaxWinsCount() + 1);
            firstPlayer.setMaxLosesCount(firstPlayer.getMaxLosesCount() + 1);
        }
        playersRepository.update(firstPlayer);
        playersRepository.update(secondPlayer);

//        System.out.println(firstPlayer.getName());
//        System.out.println((System.currentTimeMillis() - gameStartTime) / 1000); //возможный вариант без обращения к репозиторию для вывода времени игры
//        LocalDateTime.now().minus(game.getDateTime()); //возможный вариант без обращения к репозиторию для вывода времени игры
        Duration duration = Duration.between(game.getDateTime(),LocalDateTime.now());

        System.out.println("Игра длилась: " + duration.getSeconds() + " секунд");
        game.setSecondsGameTimeAmount(duration.getSeconds());
        gamesRepository.update(game);
        return null;
    }
}
