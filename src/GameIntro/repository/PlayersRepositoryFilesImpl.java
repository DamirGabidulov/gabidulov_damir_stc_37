package GameIntro.repository;

import GameIntro.models.Player;

import java.io.*;
import java.util.*;

public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private final String dbFileName;
    private final String sequenceFileName;

    public PlayersRepositoryFilesImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public Player findByNickname(String nickname) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();

            while (currentString != null) {
                String[] strings = currentString.split("#");
                if (nickname.equals(strings[2])) { //нельзя через == сравнивать, так как будет возвращать null/ так как стриги разные
                    return new Player(strings[1], strings[2], Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5]));
                    //начали с strings[1], так как первый элемент это id и его нет в конструкторе
                }
                currentString = reader.readLine();
            }
            return null;
        } catch (IOException e) {
//            throw new IllegalStateException(e);
            return null;
        }
    }

    @Override
    public void update(Player player) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();
            //оформление через мапу где ключом является айди, а значением игрок
            Map<Integer, Player> map = new HashMap<>();

            while (currentString != null) {

                String[] strings = currentString.split("#");
                if (player.getName().equals(strings[2])) {
                    map.put(Integer.parseInt(strings[0]), player);
                } else {
                    map.put(Integer.parseInt(strings[0]), new Player(strings[1], strings[2], Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5])));
                }
                currentString = reader.readLine();
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName));

            for(Map.Entry<Integer, Player> entry : map.entrySet()){
                writer.write(entry.getKey() + "#" + entry.getValue().getIp() + "#" + entry.getValue().getName() + "#" + entry.getValue().getPoints() + "#" + entry.getValue().getMaxWinsCount() + "#"
                        + entry.getValue().getMaxLosesCount() + "\n");
            }
                writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
//            return null;
        }
    }

    //оформление update через List, но при этом айди меняется у тех записей которые мы даже не меняли
    //поэтому этот метод не используем (сохранил на память)
    public void update1(Player player) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));
            String currentString = reader.readLine();
            List<Player> list = new LinkedList<>();

        while (currentString != null) {
            String[] strings = currentString.split("#");
            if (player.getName().equals(strings[2])) { //если мы нашли строчку с данным игроком (никнейм совпадает с нашей обновленной информацией),
                //то в список попадает не то что мы считали с файла а наша обновленная информация
                //то есть в список попадет только та информация которую передали в метод public void update(Player player)
                list.add(player);
            } else {
                // в противном случае в список попадет то что считали с файла
                // то есть построчно считываем файл, ищем нужного игрока, не нашли в строке файла, то записали эту строку из файла в лист
                list.add(new Player(strings[1], strings[2], Integer.parseInt(strings[3]), Integer.parseInt(strings[4]), Integer.parseInt(strings[5])));
            }
            currentString = reader.readLine();
        }
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName));
            Iterator<Player> iterator = list.iterator();

            while (iterator.hasNext()){
                Player player1 = iterator.next();
                writer.write(generateId() + "#" + player1.getIp() + "#" + player1.getName() + "#" + player1.getPoints() + "#" + player1.getMaxWinsCount() + "#"
                        + player1.getMaxLosesCount() + "\n");
            }
            writer.close();

        }  catch (IOException e) {
        throw new IllegalStateException(e);
//            return null;
            }
    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true)); // append для продолжения записи в файл
            writer.write(generateId() + "#" + player.getIp() + "#" + player.getName() + "#" + player.getPoints() + "#" + player.getMaxWinsCount() + "#"
                    + player.getMaxLosesCount() + "\n");
            writer.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Long generateId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastString = reader.readLine();
            Long id = Long.parseLong(lastString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName)); // без append для перезаписи данных
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    //мейн для проверки методов (есть комментарии)

//    public static void main(String[] args) {
//        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl("players_db.txt", "players_sequence.txt");
////        playersRepository.save(new Player("101", "nick", 10, 20,30));
////        playersRepository.save(new Player("102", "nick1", 20, 40,60));
////        playersRepository.save(new Player("103", "nick2", 30, 50,90));
//        Player player = playersRepository.findByNickname("nick"); //создаем игрока и передаем ему игрока из репо (т.е. его нужно сохранить а не просто вызвать, ни к чему же не присвается результат этой операции)
//        System.out.println(player); // переопределили toString в классе Player чтобы мог выводить информацию в sout не как ссылку а как строку
//        Player updatedPlayer = new Player("110", "nick1", -1000, 20,30);
//        playersRepository.update(updatedPlayer);
//        System.out.println(updatedPlayer);

//    }

}
