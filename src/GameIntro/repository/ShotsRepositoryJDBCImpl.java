package GameIntro.repository;

import GameIntro.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

import static GameIntro.utils.JdbcUtil.closeJdbcObjects;

public class ShotsRepositoryJDBCImpl implements ShotsRepository{

    //language=SQL
    private static final String SQL_INSERT_SHOT = "insert into " +
            "shot(datetime, game_id, shooter, target) values (?, ?, ?, ?)";

    private DataSource dataSource;

    public ShotsRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS);
            statement.setTimestamp(1,new Timestamp(System.currentTimeMillis()));
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4,shot.getTarget().getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                shot.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }

        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

}
