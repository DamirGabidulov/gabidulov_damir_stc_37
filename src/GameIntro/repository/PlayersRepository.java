package GameIntro.repository;

import GameIntro.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player);
}
