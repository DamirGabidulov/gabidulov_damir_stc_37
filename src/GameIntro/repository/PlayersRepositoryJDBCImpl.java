package GameIntro.repository;

import GameIntro.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static GameIntro.utils.JdbcUtil.closeJdbcObjects;

public class PlayersRepositoryJDBCImpl implements PlayersRepository {
    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into " +
            "player(ip, player_name, points, max_wins_count, max_loses_count)" +
            "values (?, ?, ?, ?, ?)";


    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where player_name = ?";

    //language=SQL
    private static final String SQL_SELECT_ALL_PLAYERS = "select id as player_id, * from player";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER = "update player set ip = ?, player_name = ?, points = ?, max_wins_count = ?, max_loses_count = ? where id = ?";

    static private final RowMapper<Player> playerRowMapper = row -> new Player(
            row.getLong("id"),
            row.getString("ip"),
            row.getString("player_name"),
            row.getInt("points"),
            row.getInt("max_wins_count"),
            row.getInt("max_loses_count")
    );

    private DataSource dataSource;
    public PlayersRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByNickname(String nickname) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Player player = null;
        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME);
            statement.setString(1,nickname);

            rows = statement.executeQuery();

            if (rows.next()){
                player = playerRowMapper.mapRow(rows);
            }

        } catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }return player;
    }

    @Override
    public void save(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try{
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PLAYER,Statement.RETURN_GENERATED_KEYS);


            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3,player.getPoints());
            statement.setInt(4,player.getMaxWinsCount());
            statement.setInt(5,player.getMaxLosesCount());

            //сколько строк обновили
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                player.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }


        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void update(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_PLAYER);

            statement.setString(1,player.getIp());
            statement.setString(2,player.getName());
            statement.setInt(3,player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5,player.getMaxLosesCount());
            statement.setLong(6,player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }

        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }


    }

    public List<Player> findAll(){
        Connection connection = null;
        Statement statement = null;
        ResultSet rows = null;

        List<Player> players = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            statement = connection.createStatement();
            rows = statement.executeQuery(SQL_SELECT_ALL_PLAYERS);

            while (rows.next()){
                Player player = playerRowMapper.mapRow(rows);
                players.add(player);
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }return players;
    }
}
