package GameIntro.repository;

import GameIntro.models.Shot;

import java.io.*;


public class ShotsRepositoryFileImpl implements ShotsRepository {
    private String dbFileName;
    private String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            shot.setId(generateId());
            writer.write(shot.getId() + "#" + shot.getDateTime().toString() + "#" +
                    shot.getGame().getId() + "#" + shot.getShooter().getName() + "#" + shot.getTarget().getName() + "\n"    );
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Long generateId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            // прочитали последний сгенерированный id как строку
            String lastGeneratedIdAsString = reader.readLine();
            // преобразуем ее в Long
            long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
