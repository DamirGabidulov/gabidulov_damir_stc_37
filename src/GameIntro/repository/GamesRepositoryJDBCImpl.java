package GameIntro.repository;

import GameIntro.models.Game;
import GameIntro.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

import static GameIntro.utils.JdbcUtil.closeJdbcObjects;

public class GamesRepositoryJDBCImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into " +
            "game(datetime, player_first, player_second, player_first_shots_count, player_second_shots_count, seconds_game_time_amount) " +
            "values (?, ?, ?, ?, ?, ?)";

    //language=SQL
//    private static final String SQL_FIND_BY_ID_GAME = "select * from game where id = ?";

//    //language=SQL
//    private static final String SQL_FIND_BY_ID_GAME = "select g.id as game_id, " +
//            "g.dateTime as datetime, " +
//            "p.id as player_first_id, " +
//            "p.ip as playerfirst_ip," +
//            "p.name as player_first_name," +
//            "p.points as player_first_points," +
//            "p.maxLosesCount as  " +
//            "from game as g " +
//            "join player as p on g.player_first = p.id";

    //language=SQL
        private static final String SQL_FIND_BY_ID_GAME = "select *" +
            "from game g " +
            "inner join player p on p.id = g.player_first " +
            "inner join player p2 on p2.id = g.player_second where g.id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set " +
            "datetime = ?, player_first_shots_count = ?, player_second_shots_count = ?, seconds_game_time_amount = ? where id = ?";

    //language=SQL
    private static final String SQL_FIND_BY_ID_PLAYER = "select * from player where id = ?";

    //не использую
    static private final RowMapper<Game> gameRowMapper = row -> new Game.Builder()
            .id(row.getLong("id"))
            .dateTime(row.getTimestamp("datetime").toLocalDateTime())
            .playerFirst(new Player.Builder()
                    .id(row.getLong("player_first"))
                    .build())
            .playerSecond(new Player.Builder()
                    .id(row.getLong("player_second"))
                    .build())
            .build();

    static private final RowMapper<Player> playerRowMapper = row -> new Player(
            row.getLong("id"),
            row.getString("ip"),
            row.getString("player_name"),
            row.getInt("points"),
            row.getInt("max_wins_count"),
            row.getInt("max_loses_count")
    );

    private Player findPlayerById(Long id){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        Player player = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID_PLAYER);
            statement.setLong(1, id);

            rows = statement.executeQuery();

            if (rows.next()){
                player = playerRowMapper.mapRow(rows);
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }return player;

    }

    //если без DataSource то заменить на Connection и поменять closeJdbcObjects()
    private DataSource dataSource;
    public GamesRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS);
//            statement.setTimestamp(1,new Timestamp(game.getDateTime().getNano()));
            statement.setObject(1,game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6,game.getSecondsGameTimeAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                game.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }

        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }

    }

    @Override
    public Game findById(Long gameId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Game game = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID_GAME);
            statement.setLong(1,gameId);

            rows = statement.executeQuery();

            if (rows.next()){
                game = new Game.Builder()
                        .id(rows.getLong("id"))
                        .dateTime(rows.getTimestamp("datetime").toLocalDateTime())
                        .playerFirst(findPlayerById(rows.getLong("player_first")))
                        .playerSecond(findPlayerById(rows.getLong("player_second")))
                        .playerFirstShotsCount(rows.getInt("player_first_shots_count"))
                        .playerSecondShotsCount(rows.getInt("player_second_shots_count"))
                        .secondsGameTimeAmount(rows.getLong("seconds_game_time_amount"))
                        .build();
            }
        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        } return game;
    }

    @Override
    public void update(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_GAME);
//Timestamp здесь корректно не работает поэтому обращаемся к предку Object
//            statement.setTimestamp(1,new Timestamp(System.currentTimeMillis()));
            statement.setObject(1,game.getDateTime());
            statement.setInt(2, game.getPlayerFirstShotsCount());
            statement.setInt(3,game.getPlayerSecondShotsCount());
            statement.setLong(4, game.getSecondsGameTimeAmount());
            statement.setLong(5, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }

        }catch (SQLException e){
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
    }
}
