package GameIntro.repository;

import GameIntro.models.Shot;


public interface ShotsRepository {
    void save(Shot shot);
}
