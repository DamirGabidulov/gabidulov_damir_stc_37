package GameIntro.app;

import GameIntro.dto.StatisticDto;
import GameIntro.models.Game;
import GameIntro.models.Player;
import GameIntro.repository.*;
import GameIntro.services.GameService;
import GameIntro.services.GameServiceImpl;
import GameIntro.utils.CustomDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MainSimple {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Terminal1488";

    //language=SQL
    private static final String SQL_SELECT_PLAYER = "select * from player";

    public static void main(String[] args) throws Exception {

        //было нужно до DataSource
//        Connection connection;
//        try {
//            connection = DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASSWORD);
//        } catch (SQLException throwables) {
//            throw new IllegalArgumentException(throwables);
//        }

        DataSource dataSource = new CustomDataSource(JDBC_URL, JDBC_USER, JDBC_PASSWORD);


        PlayersRepositoryJDBCImpl playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        List<Player> players = playersRepository.findAll();
        System.out.println(players);

        System.out.println(playersRepository.findByNickname("first"));


        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        System.out.println(gamesRepository.findById(1L));

        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();


        for (int j = 0; j < 3; j++) {
            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;

//            for (int j = 0; gameId < 3; gameId++) {
            int i = 0;
            while (i < 10) {

                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }
//            }


            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);

        }

//если нужно добавить игрока вручную
//        Player player = new Player("102","third",30,31,32);
//        playersRepository.save(player);
//        System.out.println(player);

//        List<Game> games = gamesRepository.save();
//        System.out.println(games);


//        Statement statement = connection.createStatement();
//        ResultSet rows = statement.executeQuery(SQL_SELECT_PLAYER);
//
//        while (rows.next()){
//            System.out.println(rows.getString("name"));
//        }
//
        //если нужно добавить игрока вручную
//        System.out.println("добавление");
//        Scanner scanner = new Scanner(System.in);
//        String firstName = scanner.nextLine();
//
//        //language=SQL
//        String insertDriver = "insert into player(name) values ('" + firstName + "')";
//        System.out.println(insertDriver);
//        statement.executeUpdate(insertDriver);
    }
}
