package homework10;

public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int elements[];

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }



    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index < count) {
            if (count == elements.length) {
                resize();
            }
            for (int i = index; i <= count; i++){
                int temp = elements[i];
                elements[i] = element;
                element = elements[i+1];
                elements[i+1] = temp;
                i++; //если убрать это поле, функция будет заменять значение по индексу
                //тогда как сейчас функция вставляет элемент по индексу и двигает массив
            }
            count++;
        } else {
            System.out.println("Неправильный индекс");
        }
    }


    //второй вариант метода с arraycopy
    public void insertSecondmethod(int index, int element) {
        if (index >= 0 && index < count) {
            if (count == elements.length) {
                resize();
            }
            int[] newArray = new int[elements.length]; //условие для метода System.arrayCopy
            // Массив newArray должен иметь достаточный размер, чтобы в нем уместились все копируемые элементы
//            System.arrayCopy(from, fromIndex, to, toIndex, count);
//            from - массив, который копируем
//            fromIndex - индекс в массиве from начиная с которого берем элементы для копирования
//            to - массив в которой копируем
//            toIndex - индекс в массиве to начиная с которого вставляем элементы
//            count - количество элементов которые берем из массива from и вставляем в массив to
            System.arraycopy(elements,0,newArray,0,index);
            newArray[index] = element;
            for (int i = index; i <= count; i++){
                newArray[i+1] = elements[i];
            }
            elements = newArray;
            count++;
        }
    }

    @Override
    public void addToBegin(int element) {
        if (count == elements.length) {
            resize();
        }
        for (int i = 0; i <= count; i++){
            int temp = elements[i];
            elements[i] = element;
            element = elements[i+1];
            elements[i+1] = temp;
            i++;
        }
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count){
            for (int i = index; i < (count - 1); i++){
                elements[i] = elements[i+1];
            }
        count--; //чтобы удалить последний элемент в массиве
        } else {
            System.out.println("Неправильный индекс");
        }
    }

    @Override
    public void remove(int element) {
            if (contains(element)){
                int index = 0;
                for (int i = 0; i < count; i++){
                    if (elements[i] == element){
                        index = i;
                    }
                }
                for (int i = index; i < (count - 1); i++){
                    elements[i] = elements[i+1];
                }
                count--;
            } else {
                System.out.println("Элемент не найден");
            }
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }


    @Override
    public boolean contains(int element) {
        for (int i = 0; i < count; i++){
            if (elements[i] == element){
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
