package homework10;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // создал список
        // если в поле ниже поменять InnoLinkedList() на InnoArrayList() будет отрабатывать уже как массив
        InnoList list = new InnoLinkedList();

        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(20);
        list.add(-77);
        list.add(100);

        System.out.println("Начальный список");
        InnoIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();


        System.out.println("Замена элемента в определенном индексе");
        list.insert(1,10);

        InnoIterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println();


        System.out.println("Добавление элемента в начало");
        list.addToBegin(1);

        InnoIterator iterator3 = list.iterator();
        while (iterator3.hasNext()) {
            System.out.print(iterator3.next() + " ");
        }
        System.out.println();


        System.out.println("Удаление по индексу");
        list.removeByIndex(2);

        InnoIterator iterator4 = list.iterator();
        while (iterator4.hasNext()) {
            System.out.print(iterator4.next() + " ");
        }
        System.out.println();

        System.out.println("Проверка наличия элемента");
        System.out.println(list.contains(-77));

        System.out.println("Удаление по значению");
        list.remove(100);

        InnoIterator iterator5 = list.iterator();
        while (iterator5.hasNext()) {
            System.out.print(iterator5.next() + " ");
        }

    }
}
