package homework10;

public class InnoLinkedList implements InnoList {
    // Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
    // Минусы - медленный доступ по  индексу

    private static class Node {
        int value;
        Node next;
        Node previous;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        Node newNode = new Node(element);
        if (index > 0 && index < count) {
            Node current = first;
            Node currentNext = current.next;
            // ссылка на второй по очередности по current (current.next это следующий)
            //то сurrentNext это current.next.next
            for (int i = 0; i < index - 1; i++) {
                current = current.next; //после последнего цикла current = index - 1
                currentNext = current.next; //после последнего цикла currentNext = index
            }
            current.next = newNode; //index
            newNode.next = currentNext; //то что было до этого на index
            count++;
        } else if (index == 0){
            addToBegin(element);
        } else {
            System.out.println("Неправильный индекс");
        }
    }

    @Override
    public void addToBegin(int element) {
        Node newNode = new Node(element);
        Node current = first;
        first = newNode;
        newNode.next = current;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count){
            Node current = first;
            Node currentNext = current.next;
            for (int i = 0; i < index - 1; i++){
                current = current.next;
                currentNext = current.next;
            }
            current.next = currentNext.next;
        } else {
            System.out.println("Неправильный индекс");
        }
    }

    @Override
    public void remove(int element) {
        Node current = first;
        Node previous = first;

        if (first.value == element) {
            first = first.next;
            count--;
            return;
        }

        for (int i = 1; i < count - 1; i++) {
            if (current.value == element) {
                previous.next = current.next;
                count--;
                return;
            }
            previous = current;
            current = current.next;
        }
        System.out.println("Элемент не найден");
    }

    public void removeSecondMethod(int element){
        Node current = first;
        Node previous = first;

        //Если current не пустой и равен элементу (т.е первый элемент), то первое число это след.элемент после текущего первого числа
        if (current != null && current.value == element){
            first = current.next;
            count--;
            return;
        }

        //Пока не пройдет весь список выполняет стандартное действие по записи ссылок prev - current - current.next
        while (current != null && current.value != element){
            previous = current;
            current = current.next;
        }

        //если равен null значит дошел до конца
        if (current == null){
            System.out.println("Элемент не найден");
            return;
        }

        //других вариантов не остается кроме как этот элемент и есть искомый
        previous.next = current.next;
        count--;
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        while (current != null){
            if (current.value == element)
                return true;
            current = current.next;
        }
        return false;
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current != null;
        }
    }
}
