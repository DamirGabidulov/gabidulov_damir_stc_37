create table player
(
    id bigserial primary key,
    ip varchar(10),
    player_name varchar(15),
    points integer,
    max_wins_count integer,
    max_loses_count integer
);

drop table player;
drop table game;

create table game
(
    id bigserial primary key,
    datetime timestamp,
    player_first bigint,
    foreign key (player_first) references player(id),
    player_second bigint,
    foreign key (player_second) references player(id),
    player_first_shots_count integer,
    player_second_shots_count integer,
    seconds_game_time_amount interval
);

create table shot
(
    id bigserial primary key,
    datetime timestamp,
    game_id bigint,
    foreign key (game_id) references game(id),
    shooter bigint,
    foreign key (shooter) references player(id),
    target bigint,
    foreign key (target) references player(id)
)